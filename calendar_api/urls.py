from django.urls import path, include
from rest_framework import routers
from calendar_api.views import EventView

router = routers.SimpleRouter()
router.register('events/', EventView)

urlpatterns = [
    path('', include(router.urls)),
]