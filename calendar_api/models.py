from django.db import models
from users.models import User
# Create your models here.


class Event(models.Model):
    hour_choices = (
        (1, '1 hour'),
        (2, '2 hours'),
        (4, '4 hours'),
        (24, '1 day'),
        (168, '1 week'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    start_datetime = models.DateTimeField()
    finish_datetime = models.DateTimeField(blank=True, null=True)
    remind_before = models.IntegerField(choices=hour_choices, blank=True, null=True, default=None)

    def save(self, *args, **kwargs):
        if not self.finish_datetime:
            self.finish_datetime = self.start_datetime.replace(hour=23, minute=59, second=59, microsecond=999)

        return super().save(*args, **kwargs)
