from django.urls import path, include
from rest_framework import routers
from rest_framework_jwt.views import refresh_jwt_token, verify_jwt_token, ObtainJSONWebToken
from users.views import UsersRegistrationView

router = routers.SimpleRouter()
router.register('registration', UsersRegistrationView)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/refresh/', refresh_jwt_token),
    path('auth/verify/', verify_jwt_token),
    path('auth/', ObtainJSONWebToken.as_view()),
]