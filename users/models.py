from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class User(AbstractUser):
    username = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(verbose_name='email', unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
